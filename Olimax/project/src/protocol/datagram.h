#ifndef _DATAGRAM_H_
#define _DATAGRAM_H_

#include <inttypes.h> //For uintxx_t definitions
#include <time.h>


#define DATAGRAM_SYNC0_BYTE 0x4A
#define DATAGRAM_SYNC1_BYTE 0x48

#define DATAGRAM_SYNC0 0
#define DATAGRAM_SYNC1 1
#define DATAGRAM_DESTHI 2
#define DATAGRAM_DESTLO 3
#define DATAGRAM_SENDERHI 4
#define DATAGRAM_SENDERLO 5
#define DATAGRAM_CMDHI 6
#define DATAGRAM_CMDLO 7
#define DATAGRAM_PAYLOAD_SIZE 8
#define DATAGRAM_DATA_CHSUMHI 9
#define DATAGRAM_DATA_CHSUMLO 10
#define DATAGRAM_HEADER_CHSUM 11
#define DATAGRAM_HEADER_LEN 12

//Size of the payload
#define DATAGRAM_MAX_PAYLOAD_SIZE 200
//Size of the from string
#define DATAGRAM_UDP_FROM_STRING 30
#define DATAGRAM_MAX_SIZE  DATAGRAM_HEADER_LEN+DATAGRAM_MAX_PAYLOAD_SIZE

/**
 * payloadHeader_t type holds information on Datagram Header
 * destination of the datagram, sender, command being transfered, etc.
 *
 * uint8_t is char
 * uint16_t is unsigned int
 */
typedef struct {
	uint8_t sync0;
	uint8_t sync1;
	uint16_t destination;
	uint16_t sender;
	uint16_t command;
	uint8_t payloadSize;

	// datagram payload checksum
	uint16_t chksum;

	// header checksum
	uint8_t hchksum;
} payloadHeader_t;

/**
 * payload_t type holds two arrays - one for header and one for payload data
 */
typedef struct {
  // header is stored as array of bytes
	uint8_t header[DATAGRAM_HEADER_LEN];

  // buffer holds payload data and is stored as array of bytes
	uint8_t data[DATAGRAM_MAX_PAYLOAD_SIZE];

} payload_t;

typedef struct {
	payload_t payload;
	uint8_t maxPayloadSize;

  // defines volume of data being stored in data buffer
  // used during processing to define whether data can be still placed
  // to the buffer, or new datagram should be prepared
	uint8_t currentPayloadSize;

	char from[DATAGRAM_UDP_FROM_STRING];

	//Flags
	uint8_t processing;
	uint8_t recieved;

	uint16_t receiveBitmask;
} datagrizzle_t;



// **************************** Datagram Header setters and getters *****************************
uint16_t datagram_get_destination(datagrizzle_t *d);
void datagram_set_destination(datagrizzle_t *d, uint16_t dest);
uint16_t datagram_get_sender(datagrizzle_t *d);
void datagram_set_sender(datagrizzle_t *d, uint16_t send);
uint16_t datagram_get_command(datagrizzle_t *d);
void datagram_set_command(datagrizzle_t *d, uint16_t cmd);
uint16_t datagram_get_chsum(datagrizzle_t *d);
void datagram_set_chsum(datagrizzle_t *d, uint16_t ch);
uint8_t datagram_get_hchsum(datagrizzle_t *d);
void datagram_set_hchsum(datagrizzle_t *d, uint8_t hch);
uint8_t datagram_get_payload_size(datagrizzle_t *d);
void datagram_set_payload_size(datagrizzle_t *d, uint8_t size);
void datagram_set_callback(datagrizzle_t *d,void(*fptr)(int, void*), void *user_data);
void datagram_set_receive_bitmask(datagrizzle_t *d, uint16_t bitMask);

// **************************** Datagram Header checksum functions *****************************
uint8_t datagram_check_header(datagrizzle_t *d);
uint8_t datagram_build_header_checksum(datagrizzle_t *d);
// **************************** Datagram Payload checksum functions *****************************
uint8_t datagram_check_fletcher(datagrizzle_t *d);
uint16_t datagram_build_fletcher_checksum(datagrizzle_t *d);

// **************************** Datagram processing functions *****************************
int16_t datagram_process(datagrizzle_t *d, uint16_t bitMask);

// **************************** Datagram printing functions *****************************
void payload_print(payload_t *p);
void datagram_print(datagrizzle_t *d);

// **************************** Datagram initialization functions *****************************
void datagram_init(datagrizzle_t *d, uint16_t maxSize);
void datagram_clear(datagrizzle_t *d);

// **************************** Datagram ADD_TYPE functions *****************************
uint8_t datagram_add_float(datagrizzle_t *d, uint16_t address, float f);
uint8_t datagram_add_int(datagrizzle_t *d, uint16_t address, uint32_t anInt);
uint8_t datagram_add_byte(datagrizzle_t *d, uint16_t address, uint8_t b);
uint8_t datagram_add_short(datagrizzle_t *d, uint16_t address, uint16_t s);
uint8_t datagram_add_string(datagrizzle_t *d, uint16_t address, char *s);

// **************************** Datagram Sending functions *****************************
uint8_t datagram_pack(datagrizzle_t *d);
uint8_t datagram_data(datagrizzle_t *d, uint8_t *buf, uint8_t start, uint8_t len);



#endif

