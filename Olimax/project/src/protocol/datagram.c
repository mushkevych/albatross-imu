#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifdef BIG_ENDIAN
	#include <netinet/in.h>
#else
	#define ntohs(x)
	#define ntohl(x)
	#define htonl(x)
	#define htons(x)
#endif

#include "albatross_std.h"
#include "datagram.h"
#include "protocol.h"
#include "71x_type.h"

// **************************** Datagram Header setters and getters *****************************
//Datagram Header setters and getters to overcome endianess issues.
//The data is sent in network byte order (big endian). Because arm
//and PPC are also bigendian ntohx() and htonx() are no-ops so processing is
//fastest on these systems.
uint16_t datagram_get_destination(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_DESTHI] << 8));
	ret |= d->payload.header[DATAGRAM_DESTLO];
	return ret;
}
void datagram_set_destination(datagrizzle_t *d, uint16_t dest)
{
	d->payload.header[DATAGRAM_DESTHI] = HI_BYTE(dest);
	d->payload.header[DATAGRAM_DESTLO] = LO_BYTE(dest);
}

uint16_t datagram_get_sender(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_SENDERHI] << 8));
	ret |= d->payload.header[DATAGRAM_SENDERLO];
	return ret;
}
void datagram_set_sender(datagrizzle_t *d, uint16_t send)
{
	d->payload.header[DATAGRAM_SENDERHI] = HI_BYTE(send);
	d->payload.header[DATAGRAM_SENDERLO] = LO_BYTE(send);
}

// command being transferred
uint16_t datagram_get_command(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_CMDHI] << 8));
	ret |= d->payload.header[DATAGRAM_CMDLO];
	return ret;
}
void datagram_set_command(datagrizzle_t *d, uint16_t cmd)
{
	d->payload.header[DATAGRAM_CMDHI] = HI_BYTE(cmd);
	d->payload.header[DATAGRAM_CMDLO] = LO_BYTE(cmd);
}

// check sum for the datagram payload
uint16_t datagram_get_chsum(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_DATA_CHSUMHI] << 8));
	ret |= d->payload.header[DATAGRAM_DATA_CHSUMLO];
	return ret;
}
void datagram_set_chsum(datagrizzle_t *d, uint16_t ch)
{
	d->payload.header[DATAGRAM_DATA_CHSUMHI] = HI_BYTE(ch);
	d->payload.header[DATAGRAM_DATA_CHSUMLO] = LO_BYTE(ch);
}

// check sum for the datagram's header
uint8_t datagram_get_hchsum(datagrizzle_t *d)
{
	return d->payload.header[DATAGRAM_HEADER_CHSUM];
}
void datagram_set_hchsum(datagrizzle_t *d, uint8_t hch)
{
	d->payload.header[DATAGRAM_HEADER_CHSUM] = hch;
}

// actual payload size, transferred by datagram
uint8_t datagram_get_payload_size(datagrizzle_t *d)
{
	return d->payload.header[DATAGRAM_PAYLOAD_SIZE];
}
void datagram_set_payload_size(datagrizzle_t *d, uint8_t size)
{
	d->payload.header[DATAGRAM_PAYLOAD_SIZE] = size;
}

// datagram bitmask is used to comparing the dest field and the bitMask
// and define whether datagrams is for us
// @see datagram_process
void datagram_set_receive_bitmask(datagrizzle_t	*d, uint16_t bitMask)
{
	d->receiveBitmask = bitMask;
}



// **************************** Datagram Header checksum functions *****************************

//Checks the header for validity, including checking
//the Sync bytes, and the HCHSUM checksum for validity
uint8_t datagram_check_header(datagrizzle_t *d)
{
	uint8_t j = 0;
	bool_t crcCheck = FALSE;
	bool_t headerCheck = FALSE;

	//Check Sync Bytes
	if((d->payload.header[DATAGRAM_SYNC0] == DATAGRAM_SYNC0_BYTE) && (d->payload.header[DATAGRAM_SYNC1] == DATAGRAM_SYNC1_BYTE))
		headerCheck = TRUE;
	else
		headerCheck = FALSE;

	//8-bit XOR checksum
	j = datagram_build_header_checksum(d);

	if(j == d->payload.header[DATAGRAM_HEADER_CHSUM])
		crcCheck = TRUE;
	else
		crcCheck = FALSE;

	//Both have to be TRUE for a valid header
	return (headerCheck && crcCheck ? TRUE : FALSE);
}

// Builds a header checksum for a datagram.
// Should be called prior to sending
// Returns the built checksum
uint8_t datagram_build_header_checksum(datagrizzle_t *d)
{
	uint8_t crc, i;
	uint8_t sizeOfHeader = DATAGRAM_HEADER_LEN-1;
	crc = 0;

	//XOR Checksum
	for(i = 0; i < sizeOfHeader; i++)
		crc ^= d->payload.header[i];

	return crc;
}

// **************************** Datagram Payload checksum functions *****************************

//Performs a fletcher check on the datagram payload, from the
//start address of buffer upto size number of bytes.
//Returns TRUE for succes
uint8_t datagram_check_fletcher(datagrizzle_t *d)
{
	uint16_t j,k = 0;

	//Calculated
	j = datagram_build_fletcher_checksum(d);

	//Expected - Note its sent in beg endial so use setters and getters
	k = datagram_get_chsum(d);

	//Compare and return
	return (j == k ? TRUE : FALSE);
}

//Builds a datagram payload fletcher checksum. Should be used to populate the header
//struct prior to sending
//Returns the built checksum
uint16_t datagram_build_fletcher_checksum(datagrizzle_t *d)
{
	uint16_t i = 0;
	uint16_t j = 0;
	uint8_t end;
	uint16_t s1 = 1;
	uint16_t s2 = 0;

	//Fletcher Computation
	end = datagram_get_payload_size(d);
	for(i = 0; i < end; i++)
	{
		s1 += d->payload.data[i];
		s1 %= 255;
		s2 += s1;
		s2 %= 255;
	}
	//Computed
	j = ((uint16_t)s2 * 256) + (uint16_t)s1;
	//printf("\tComputed Fletcher = 0x%X\n",j]);

	return j;
}

// **************************** Datagram processing functions *****************************

//Checks if the datagram is valid, which includes
//1) Checking the header is valid (XOR)
//2) Checking this is the corect destination by comparing the dest field and the bitMask
//3) Checking the payload is valid (CRC/Fletcher)
//If The header is valid and the datagram is destined for us (the bitmask destination matches)
//The payload is processed and the following information is filled out into the datagram.
//1) Lightweight Command Field
//2) Datagram Destination
//3) Payload Size
//Note that the contents of the payload are not copied accross here because that would be a
//wasted operation if the datagram specified a lightweight command.
//Returns the cmd (-1 if invalid or not addressed to us)
int16_t datagram_process(datagrizzle_t *d, uint16_t bitMask)
{
	uint8_t error = FALSE;

	//Fist check the header XOR CRC
	if(datagram_check_header(d))
	{
		//printf("Header OK\n");
		//Check if its addressed to us before we go any further
		if(datagram_get_destination(d) & bitMask)
		{
			//printf("Datagram is addressed OK\n");
			//Its addressed to us
			//Check the payload is valid (Fletcher)
			if(datagram_check_fletcher(d))
			{
				//printf("Fletcher OK\n")
				;//The datagram is valid
			}
			else
			{
				error = TRUE; //Payload Fail
				//fprintf(stderr, "Payload Fletcher Checksum is wrong\n");
		  }
		}
		else
		{
			error = TRUE; //Not addressed to us
		}
	}
	else
	{
		error = TRUE; //Header Fail
  	//fprintf(stderr, "Header CRC is wrong\n");
	}

	//Return -1 for error, or the command
	return (error ? -1 : datagram_get_command(d));
}


// **************************** Datagram printing functions *****************************

// Prints the payload information out
void payload_print(payload_t *p)
{
	uint8_t i, endPayload;

	printf("Payload:\n");
	printf("\tHeader:\n");

	for(i = 0; i <= DATAGRAM_HEADER_CHSUM; i++)
	{
		printf("[0x%X]", p->header[i]);
	}

	printf("\n\tData:\n");
	endPayload = p->header[DATAGRAM_PAYLOAD_SIZE];
	for(i = 0; i < endPayload; i++)
	{
		printf("[0x%X]", p->data[i]);
	}

	printf("\nDone!\n");
}

//Prints a datagram
void datagram_print(datagrizzle_t *d)
{
	uint8_t i;
	uint8_t endPayload;

	printf("Datagram Contents:\n");
	printf("\t[0x%X] Destination\n",datagram_get_destination(d));
	printf("\t[0x%X] Sender\n",datagram_get_sender(d));
	printf("\t[0x%X] Command\n",datagram_get_command(d));
	printf("\t[0x%X] Payload Size\n",datagram_get_payload_size(d));
	printf("\t[0x%X] Header Checksum\n",datagram_get_hchsum(d));
	printf("\t[0x%X] Payload Checksum\n",datagram_get_chsum(d));
	printf("\tBegin Payload: ");

	endPayload = datagram_get_payload_size(d);
	for(i = 0; i < endPayload; i++)
	{
		printf("[0x%X]", d->payload.data[i]);
	}

	printf("\n\tEnd Payload\n");
	printf("End Datagram\n");

}

// **************************** Datagram initialization functions *****************************

// Initialises the datagram
void datagram_init(datagrizzle_t *d, uint16_t maxSize)
{
	d->maxPayloadSize = maxSize;
	d->currentPayloadSize = 0;

	//Threading stuff
	d->recieved = FALSE;
	d->processing = FALSE;
}

// clears datagram
void datagram_clear(datagrizzle_t *d)
{
	uint8_t i, sizeOfHeader;
	sizeOfHeader = DATAGRAM_HEADER_LEN;

  // clears header
	for(i = 0; i < sizeOfHeader; i++)
	{
		d->payload.header[i] = 0x0;
	}

	// clears data
	for(i = 0; i < DATAGRAM_MAX_PAYLOAD_SIZE; i++)
	{
		d->payload.data[i] = 0x0;
	}

	// reset fields
	d->currentPayloadSize = 0;
	d->recieved = FALSE;
	d->processing = FALSE;
	d->receiveBitmask = 0;

}

// **************************** Datagram ADD_TYPE functions *****************************

//adds a float to the datagram.
uint8_t datagram_add_float(datagrizzle_t *d, uint16_t address, float f)
{
	uint8_t i,len = 0;
	uint32_t asFloat;
	i = d->currentPayloadSize;
	len = sizeof(float);

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address [AddyHigh][AddyLow]
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		asFloat = *((uint32_t *)&f);

		//printf("Data as float=%f, uint32 = 0x%X\n",f,asFloat);
		d->payload.data[i++] = LONG_HI_HI_BYTE(asFloat);
		d->payload.data[i++] = LONG_HI_BYTE(asFloat);
		d->payload.data[i++] = LONG_LO_BYTE(asFloat);
		d->payload.data[i++] = LONG_LO_LO_BYTE(asFloat);

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a byte to the datagram.
uint8_t datagram_add_byte(datagrizzle_t *d, uint16_t address, uint8_t b)
{
	uint8_t i,len = 0;
	i = d->currentPayloadSize;
	len = sizeof(uint8_t);

	//printf("Adding Byte=0x%X @ index %d, to address 0x%X\n",b,i,address);
	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		d->payload.data[i] = b;

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a short to the datagram.
uint8_t datagram_add_short(datagrizzle_t *d, uint16_t address, uint16_t s)
{
	uint8_t i,len = 0;
	i = d->currentPayloadSize;
	len = sizeof(uint16_t);

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		d->payload.data[i++] = HI_BYTE(s);
		d->payload.data[i++] = LO_BYTE(s);

		//update the payload struct to reflect how full the payload buffer is
    d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a float to the datagram.
uint8_t datagram_add_int(datagrizzle_t *d, uint16_t address, uint32_t anInt)
{
	uint8_t i,len = 0;

	i = d->currentPayloadSize;
	len = sizeof(uint32_t);

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address [AddyHigh][AddyLow]
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		d->payload.data[i++] = LONG_HI_HI_BYTE(anInt);
		d->payload.data[i++] = LONG_HI_BYTE(anInt);
		d->payload.data[i++] = LONG_LO_BYTE(anInt);
		d->payload.data[i++] = LONG_LO_LO_BYTE(anInt);

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a string to the datagram.
uint8_t datagram_add_string(datagrizzle_t *d, uint16_t address, char *s)
{
	uint8_t i,len = 0;
	i = d->currentPayloadSize;

	//Get the length of the string
	len = strlen(s) + 1; //Add 1 for null termination

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Copy from s into the buffer (including the null char)
		strcpy(&d->payload.data[i],s);

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}


// **************************** Datagram Sending functions *****************************


// Prepares a datagram for sending
// datagram must not be updated/reused before being send
uint8_t datagram_pack(datagrizzle_t *d)
{
	//Fill out the sync bytes
	d->payload.header[DATAGRAM_SYNC0] = DATAGRAM_SYNC0_BYTE;
	d->payload.header[DATAGRAM_SYNC1] = DATAGRAM_SYNC1_BYTE;
	//The dest and command and sender are already filled out providing
	//the getters and setters have been used

	//Fill out the payload size
	datagram_set_payload_size(d, d->currentPayloadSize);

	//Generate the complete checksum
	datagram_set_chsum(d, datagram_build_fletcher_checksum(d));

	//Header Checksum
	datagram_set_hchsum(d, datagram_build_header_checksum(d));

	d->currentPayloadSize = 0;
	return TRUE;

}

/**
 * 
 */
uint8_t datagram_data(datagrizzle_t *d, uint8_t *buf, uint8_t start, uint8_t len){
               
	uint32_t i,j; 
        uint32_t sizeOfHeader = DATAGRAM_HEADER_LEN;
	uint32_t sizeOfPayload = datagram_get_payload_size(d);
        uint32_t sizeOfDatagram = sizeOfHeader + sizeOfPayload;

        if(start+len >= sizeOfDatagram){
               len = sizeOfDatagram - start; 
        }

        // copying data
        for (i = start, j=0; i < sizeOfHeader && j < len; i++,j++){
                buf[j] = d->payload.header[i];
        }

        for (i=0; i < sizeOfPayload && j < len; i++,j++){
                buf[j] = d->payload.data[i];
        } 

        return j;
}
