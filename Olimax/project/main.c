/******************** (C) COPYRIGHT 2007 STMicroelectronics ********************
* File Name          : main.c
* Author             : MCD Application Team
* Version            : V4.0
* Date               : 10/09/2007
* Description        : Main program body
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "71x_lib.h"
#include "71x_it.h"

#include "datagram.h"
#include "protocol.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UART0_Rx_Pin (0x0001 << 8) /* TQFP 64: pin N� 63, TQFP 144 pin N� 143 */
#define UART0_Tx_Pin (0x0001 << 9) /* TQFP 64: pin N� 64, TQFP 144 pin N� 144 */
#define UART1_Rx_Pin (0x0001 << 10) /* TQFP 64: pin N� 63, TQFP 144 pin N� 143 */
#define UART1_Tx_Pin (0x0001 << 11) /* TQFP 64: pin N� 64, TQFP 144 pin N� 144 */




/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u8 bRecieveU0 = 0;
u8 bRecieveU1 = 0;
u8 bRByte;

//spi IMU9DOF
u16 spi_data_addresses[29] = {0,   
    STATE_D_ROLL_ANGLE, STATE_D_PITCH_ANGLE, STATE_D_YAW_ANGLE, STATE_D_HEADING_MAGNETIC,5,6,7,8,9,
    STATE_D_GYRO_X, STATE_D_GYRO_Y, STATE_D_GYRO_Z, 
    STATE_D_ACCELERATION_X, STATE_D_ACCELERATION_Y, STATE_D_ACCELERATION_Z,16,17,18,19,
    STATE_D_DCM00, STATE_D_DCM01, STATE_D_DCM02, 
    STATE_D_DCM10, STATE_D_DCM11, STATE_D_DCM12, 
    STATE_D_DCM20, STATE_D_DCM21, STATE_D_DCM22
};
u8 spi_data[SPI_BUF_LEN];
u8 *spi_buf = spi_data;
vu32 spi_data_index = 0;
u8 cross_count = 0;


//uart gamstix
static u8 uart_data[DATAGRAM_MAX_SIZE];
static u8 uart_data_len = 0;
static u8 uart_data_index = 0;
static u8 uart_data_status = 0;


/* Private function prototypes -----------------------------------------------*/
  void Clock_Configuration(void);
  void Pins_Configuration(void);
  void UART_Configuration(void);
  void BSPI_Configuration (void);
  void ADC_Configuration (void);
  u16 ADC12_ConversionAverage(ADC12_Channels Channel, u16 N);
  u16 ADC12_Read(ADC12_Channels ch, u32* value, u32* counter);
  void SendHexToUART(u32 data);
  
  void DatagramInit(datagrizzle_t *d);
  void DatagramAddFloat(datagrizzle_t *d, uint16_t address, u8* data);
/* Extern function prototypes ------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : main
* Description    : Main program
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
int main(void)
{
  #ifdef DEBUG
  debug();
  #endif
  
  Clock_Configuration();
  
  /* GPIO configuration ------------------------------------------------------*/
  Pins_Configuration();

  /* UART configuration ----------------------------------------------------- */
  UART_Configuration();
  
  /* BSPI configuration ----------------------------------------------------- */
  BSPI_Configuration();

  /* ACD configuration */
  ADC_Configuration ();

  //GPIO_BitWrite(GPIO1, 15, 1); // set RESET accel to 1
  //GPIO_BitWrite(GPIO1, 15, 0); // set RESET accel to 0
  //for (i=0;i<255;i++);
  //GPIO_BitWrite(GPIO1, 15, 1); // set RESET accel to 1
	
  //u32 adcCH1Value=0; u32 adcCH1Count=0;
  //u32 adcCH2Value=0; u32 adcCH2Count=0;
  //u32 adcCH3Value=0; u32 adcCH3Count=0;
  //u32 adcCH4Value=0; u32 adcCH4Count=0;
  
  for (u32 i=0; i<SPI_BUF_LEN; i++){
    spi_data[i] = 0xff;
  }

  datagrizzle_t d; 
  
	
  while(1) //Infinite loop
  { 
    /* 
    //hello
    adcCH1Value=0; adcCH1Count=0;
    adcCH2Value=0; adcCH2Count=0;
    adcCH3Value=0; adcCH3Count=0;
    adcCH4Value=0; adcCH4Count=0;
    
    for(u32 i=0; i< 100; i++){
        ADC12_Read(ADC12_CHANNEL0, &adcCH1Value, &adcCH1Count);
        ADC12_Read(ADC12_CHANNEL1, &adcCH2Value, &adcCH2Count);
        ADC12_Read(ADC12_CHANNEL2, &adcCH3Value, &adcCH3Count);
        ADC12_Read(ADC12_CHANNEL3, &adcCH4Value, &adcCH4Count);
    }
    
    //calculate avarages
    adcCH1Value /=adcCH1Count;
    adcCH2Value /=adcCH2Count;
    adcCH3Value /=adcCH3Count;
    adcCH4Value /=adcCH4Count;    
   
    SendHexToUART(adcCH1Value);
    SendHexToUART(adcCH2Value);
    SendHexToUART(adcCH3Value);
    SendHexToUART(adcCH4Value);
		
    if (bRecieveU0 == 1)
    {
        bRecieveU0 = 0;
        UART_ByteSend(UART1, &bRByte);            
    }    
    if (bRecieveU1 == 1)
    {
        bRecieveU1 = 0;
        UART_ByteSend(UART0, &rbyte);            
    }
    */
    
    static u8 batch_index = 0;
    static u32 actual_index = 0;
    if(cross_count != 0 && spi_data_index > actual_index){ //data overwritten -> reset
        batch_index = 0;
        cross_count = 0;
        actual_index = spi_data_index;                    
    }
    
    if(cross_count*SPI_BUF_LEN +spi_data_index > actual_index){
      u8 end_index = spi_data_index>=actual_index?spi_data_index:SPI_BUF_LEN;              
      s8 delta = end_index - actual_index; 
      if(batch_index == 5 && delta >=1 && spi_data[actual_index] != 1){
        batch_index = 4;
      }else if( batch_index >= 5 && //add data to datagram      
                delta >= 5){  //have part of data transmitted already
                  
        u8 address_index = spi_data[actual_index];        
        
        if(address_index==0 || address_index >=29){
            batch_index = 0;
        }else{
            if(address_index == 1){
              DatagramInit(&d);
            }
            
            //if(address_index >20)
            DatagramAddFloat(&d, spi_data_addresses[address_index], spi_buf+actual_index+1);                          
            batch_index+=5;
            actual_index+=5;            
            
            if(batch_index == SPI_BATCH_LEN){
              batch_index = 0;
              cross_count = 0;
              
              datagram_pack(&d);                       
              uart_data_len = datagram_data(&d, uart_data, 0, DATAGRAM_MAX_SIZE);  
              uart_data_index = 0;
              uart_data_status = 1;          
            }
        }
        
      }else if(batch_index < 5){ // seek for header
        
        while(actual_index < end_index){           
          if(spi_data[actual_index] == 0){
            batch_index++;
          }else{
            batch_index = 0;
          }          
          actual_index++;

          if(batch_index == 5){
            break;
          }

        }
      }
      
      if(actual_index == SPI_BUF_LEN){
        actual_index = 0;
        cross_count--;
      }      
      
    }

    if(uart_data_status==1){ //send datagram part to uart
        u8 len = uart_data_len - uart_data_index;
        if(len > 0){
            len = len> 10 ? 10:len;
            UART_DataSend(UART0, uart_data+uart_data_index, len);
            uart_data_index+=len;
        }
        if(uart_data_index >= uart_data_len){ //datagram sent
            uart_data_index = 0;
            uart_data_len = 0;
            uart_data_status = 0;
        }                    
    }
  }  
}

void DatagramInit(datagrizzle_t *d){
    datagram_clear(d);	//Sender
    datagram_init(d,DATAGRAM_MAX_PAYLOAD_SIZE);
    datagram_set_destination(d, AUTOPILOT_D_BITMASK|COMM_D_BITMASK);
    datagram_set_command(d,LWC_DATA);
    datagram_set_sender(d, STATE_D_BITMASK);
}

void DatagramAddFloat(datagrizzle_t *d, uint16_t address, u8* data){
    //u8 b;
    //b = data[0];data[0] = data[3];data[3] = b;b = data[1];data[1] = data[2];data[2] = b;
    //datagram_add_float(d, address, int2Float(data)); 

    int32_t dd;
    dd  = (uint32_t)(0xFF000000 & (data[3]<<24));
    dd |= (uint32_t)(0x00FF0000 & (data[2]<<16));
    dd |= (uint32_t)(0x0000FF00 & (data[1]<<8));
    dd |= (uint32_t)( 0x000000FF &  data[0]);

    //datagram_add_int(d, address, dd);
    datagram_add_float(d, address, (float)(dd*0.0000001));
}

void SendHexToUART(u32 data){
    u8 hex2char[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    u8 msb = (data>>8)&0xff;
    u8 lsb = data&0xff;
    u8 b;
    
	b = '0';
        UART_ByteSend(UART0, &b);
	b = 'x';
	UART_ByteSend(UART0, &b);
	b = hex2char[(msb>>4)&0x0f];
	UART_ByteSend(UART0, &b);
	b = hex2char[(msb)&0x0f];
	UART_ByteSend(UART0, &b);
	b = hex2char[(lsb>>4)&0x0f];
	UART_ByteSend(UART0, &b);
	b = hex2char[(lsb)&0x0f];
	UART_ByteSend(UART0, &b);
}

/*******************************************************************************
* Function Name  : Clock_Configuration
* Description    : Internal clock cirquits configuration
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
  void Clock_Configuration(void)
  {
    /* System clocks configuration ------------------------------------------*/
    /* MCLK = PCLK1 = PCLK2 = 24MHz*/ 
    
    /* Configure PCLK1 = RCLK / 1 */
    RCCU_PCLK1Config (RCCU_DEFAULT);

    /* Configure PCLK2 = RCLK / 1 */
    RCCU_PCLK2Config (RCCU_DEFAULT);

    /* Configure MCLK clock for the CPU, RCCU_DEFAULT = RCLK /1 */
    RCCU_MCLKConfig (RCCU_DEFAULT);
   
    /* Configure the PLL1 ( * 12 , / 4 ) */
    //RCCU_PLL1Config (RCCU_PLL1_Mul_12, RCCU_Div_4) ;
    RCCU_PLL1Config (RCCU_PLL1_Mul_16, RCCU_Div_4) ;

    while(RCCU_FlagStatus(RCCU_PLL1_LOCK) == RESET)
    {
      /* Wait PLL to lock */
    }
  
    /* Select PLL1_Output as RCLK clock */
    RCCU_RCLKSourceConfig (RCCU_PLL1_Output) ;
  
    /* Enable UART0 clock on APB1 */
    APB_ClockConfig (APB1, ENABLE, UART0_Periph );
    APB_ClockConfig (APB1, ENABLE, UART1_Periph );
    
    /* Enable GPIO0 clock on APB2 */
    APB_ClockConfig (APB2, ENABLE, GPIO0_Periph );
    
    /* Enable BSPI0 and BSPI1 clocks on APB1 */
    APB_ClockConfig (APB1, ENABLE, BSPI0_Periph );
    
    /* Enable GPIO0 clock on APB2 */
    APB_ClockConfig (APB2, ENABLE, GPIO0_Periph );
		
    /* Enable ADC12 and GPIO0 clocks on APB2 */
    APB_ClockConfig (APB2, ENABLE, ADC12_Periph | GPIO1_Periph | GPIO0_Periph);
  };

/*******************************************************************************
* Function Name  : Pins_Configuration
* Description    : Configure Pins dedication
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
  void Pins_Configuration(void)
  {
    /*  Configure the GPIO pins for UART0 -----------------------------------*/
    GPIO_Config(GPIO0, UART0_Tx_Pin, GPIO_AF_PP);
    GPIO_Config(GPIO0, UART0_Rx_Pin, GPIO_IN_TRI_CMOS);
		
    /*  Configure the GPIO pins for UART1 -----------------------------------*/
    GPIO_Config(GPIO0, UART1_Tx_Pin, GPIO_AF_PP);
    GPIO_Config(GPIO0, UART1_Rx_Pin, GPIO_IN_TRI_CMOS);
    
    /*  Configure the GPIO pins for BSPI -------------------------------------*/
    /* Configure MOSIx, MISOx, and SCLKx pins as Alternate function Push Pull */
    GPIO_Config (GPIO0, 0x0007, GPIO_AF_PP);
    
    /* Configure nSSx pins mode as Input Tristate CMOS */  
    GPIO_Config (GPIO0, 0x0088, GPIO_IN_TRI_CMOS);
		
    GPIO_Config (GPIO0, 0x0010, GPIO_OUT_PP);
    GPIO_Config (GPIO1, 0x8000, GPIO_OUT_PP);
		
    /*  Configure the Analog input AN3 as HI_AIN_TRI */
    //GPIO_Config (GPIO1, (0x0001 << 3), GPIO_HI_AIN_TRI);
    /*  Configure the Analog input AN0 - AN3 as HI_AIN_TRI */
    GPIO_Config (GPIO1, 0x000F, GPIO_HI_AIN_TRI);
  }
  

 
/*******************************************************************************
* Function Name  : UART_Configuration
* Description    : Configure UART0
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
  void UART_Configuration(void)
  {
    /* Configure the UART X */
    /* Turn UART0 on */
    UART_OnOffConfig(UART0, ENABLE);
    UART_OnOffConfig(UART1, ENABLE); 
  
    /* Disable FIFOs */      
    UART_FifoConfig (UART0, DISABLE);
    UART_FifoConfig (UART1, DISABLE);
  
    /* Reset the UART_RxFIFO */      
    UART_FifoReset (UART0 , UART_RxFIFO);
    UART_FifoReset (UART1 , UART_RxFIFO);
		
    /* Reset the UART_TxFIFO */ 
    UART_FifoReset (UART0 , UART_TxFIFO);
    UART_FifoReset (UART1 , UART_TxFIFO);
		
    /* Disable Loop Back */ 
    UART_LoopBackConfig(UART0, DISABLE); 
    UART_LoopBackConfig(UART1, DISABLE);
 
    /* Configure the UART0 as following:
                              - Baudrate = 9600 Bps
                              - No parity
                              - 8 data bits
                              - 1 stop bit */
    //UART_Config(UART0, 9600, UART_NO_PARITY, UART_1_StopBits, UARTM_8D);
    UART_Config(UART0, 38400, UART_NO_PARITY, UART_1_StopBits, UARTM_8D);
    UART_Config(UART1, 38400, UART_NO_PARITY, UART_1_StopBits, UARTM_8D);
  
    /*  Enable Rx */
    UART_RxConfig(UART0, ENABLE);
    UART_RxConfig(UART1, ENABLE);
          
    /* EIC configuration -----------------------------------------------------*/

    /* Configure the EIC channel interrupt */
    EIC_IRQChannelPriorityConfig(UART0_IRQChannel, 1);
    EIC_IRQChannelConfig(UART0_IRQChannel, ENABLE);
    //EIC_IRQConfig(ENABLE);
		/* Configure the EIC channel interrupt */
    EIC_IRQChannelPriorityConfig(UART1_IRQChannel, 2);
    EIC_IRQChannelConfig(UART1_IRQChannel, ENABLE);
    EIC_IRQConfig(ENABLE);

    UART_ItConfig(UART0, UART_RxBufNotEmpty, ENABLE);
    UART_ItConfig(UART1, UART_RxBufNotEmpty, ENABLE);
  }
  
/*******************************************************************************
* Function Name  : UART_Configuration
* Description    : Configure UART0
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
  void BSPI_Configuration (void)
  {
    /* Enable the BSPI0 interface */
    BSPI_BSPI0Conf (ENABLE);

    /* Initialize BSPI0 */
    BSPI_Init (BSPI0);

    /* Configure Baud rate Frequency: ---> APB1/6 */
    // BSPI_ClockDividerConfig (BSPI0, 6);
    //BSPI_ClockDividerConfig (BSPI0, 8);

    /* Configure BSPI0 as a Slave */
    BSPI_MasterEnable (BSPI0, DISABLE);

    /* Configure the clock to be active high */
    BSPI_ClkActiveHigh (BSPI0, ENABLE);

    /* Enable capturing the first Data sample on the first edge of SCK */
    BSPI_ClkFEdge (BSPI0, ENABLE);

    /* Set the word length to 8 bit */
    BSPI_8bLEn (BSPI0, ENABLE);

    /*  Configure the depth of transmit to 1 word */
    BSPI_TrFifoDepth (BSPI0, 1);
    
    BSPI_RcFifoDepth (BSPI0, 5);
    
  
    BSPI_RcItSrc(BSPI0, BSPI_RC_FF);
    EIC_IRQChannelPriorityConfig(SPI0_IRQChannel, 3);	
    EIC_IRQChannelConfig (SPI0_IRQChannel, ENABLE);
    EIC_IRQConfig(ENABLE);
		
    /* Enable BSPI0 */
    BSPI_Enable (BSPI0 , ENABLE);
    
  }
	
/*******************************************************************************
* Function Name  : ADC_Configuration
* Description    : Configure ADC3
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/  
	void ADC_Configuration (void)
	{
		/* ADC12 configuration ---------------------------------------------------*/

  	/*  Initialize the conveter register. */
  	ADC12_Init();

  	/* Configure the sampling frequency = 500Hz */
  	ADC12_PrescalerConfig(500);

  	/*  Select the single conversion mode */
  	//ADC12_ModeConfig (ADC12_SINGLE);
 	
    /* Select the Channel 3 to be converted in single channel mode*/
  	//ADC12_ChannelSelect(ADC12_CHANNEL3);

    /* Round Robin mode */
  	ADC12_ModeConfig (ADC12_ROUND);    



  	/*  Start the Converter */
  	ADC12_ConversionStart();
	}
	
/*******************************************************************************
* Function Name  : ADC12_Conversion_Average
* Description    : This function is used to compute the average of an ADC 
*                  channel conversion    
* Input1         : ADC12_Channel: channel selected to be converted
* Input2         : u16 : number of time the channel is converted
* Output         : None
* Return         : Conversion average result 
*******************************************************************************/
u16 ADC12_ConversionAverage(ADC12_Channels Channel, u16 N)
{
  u16 i = 0;
  ADC12_Flags ChannelFlag;
  u32 Conv_Sum = 0;
  u16 Conv_Av =0;

  ChannelFlag = (ADC12_Flags)(1 << (Channel >> 4)) ;

  for(i = 0; i<N; i++)
  {
    /* Wait until the availabilty of converted data */
    while(ADC12_FlagStatus( ChannelFlag) == RESET);

    /* Get the conversion result and add it to Conv_Sum*/
    Conv_Sum += ADC12_ConversionValue(Channel);
   }
  
  Conv_Av = Conv_Sum / N;
 
  return(Conv_Av);
}

u16 ADC12_Read(ADC12_Channels ch, u32* value, u32* counter){


    /* Test if the conversion of the channel has finished and no overrun */
    /* if was overrun probably it will be needed to stop and start conversion process again */
    /*
    ADC12_Flags chf = (ADC12_Flags)(1 << (ch >> 4)) ;
    if((ADC12_FlagStatus (chf)) == RESET)// || (ADC12_FlagStatus(ADC12_OR) != RESET))
		{
        return 0;
    }
    */
    *value += ADC12_ConversionValue(ch);
    *counter +=1;
    return 1;
}
	

/******************* (C) COPYRIGHT 2007 STMicroelectronics *****END OF FILE****/
