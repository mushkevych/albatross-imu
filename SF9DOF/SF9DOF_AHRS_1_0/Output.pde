
void printdata(void)
{    
      Serial.print("!");

      #if PRINT_EULER == 1
      Serial.print("ANG:");
      Serial.print(ToDeg(roll));
      Serial.print(",");
      Serial.print(ToDeg(pitch));
      Serial.print(",");
      Serial.print(ToDeg(yaw));
      //Serial.print(",");
      //Serial.print(ToDeg(MAG_Heading));
      #endif      
      #if PRINT_ANALOGS==1
      Serial.print(",AN:");
      Serial.print(read_adc(0));
      Serial.print(",");
      Serial.print(read_adc(1));
      Serial.print(",");
      Serial.print(read_adc(2));  
      Serial.print(",");
      Serial.print(accel_x);
      Serial.print (",");
      Serial.print(accel_y);
      Serial.print (",");
      Serial.print(accel_z);
      Serial.print(",");
      Serial.print((int)magnetom_x);
      Serial.print (",");
      Serial.print((int)magnetom_y);
      Serial.print (",");
      Serial.print((int)magnetom_z);      
      #endif
      #if PRINT_DCM == 1
      Serial.print (",DCM:");
      Serial.print(convert_to_dec(DCM_Matrix[0][0]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[0][1]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[0][2]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[1][0]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[1][1]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[1][2]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[2][0]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[2][1]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[2][2]));
      #endif
      Serial.println();    
}

void printdata_spi(void){
      spi_transfer(0, 0);
        
      #if PRINT_EULER == 1
      spi_transfer(1, ToDeg(roll));
      spi_transfer(2, ToDeg(pitch));
      spi_transfer(3, ToDeg(yaw));
      spi_transfer(4, ToDeg(MAG_Heading));
      #endif      
      
      #if PRINT_ANALOGS==1
      spi_transfer(10, read_adc(0));
      spi_transfer(11, read_adc(1));
      spi_transfer(12, read_adc(2));  
      spi_transfer(13, accel_x);
      spi_transfer(14, accel_y);
      spi_transfer(15, accel_z);
      spi_transfer(16, magnetom_x);
      spi_transfer(17, magnetom_y);
      spi_transfer(18, magnetom_z);      
      #endif
      
      #if PRINT_DCM == 1
      spi_transfer(20, DCM_Matrix[0][0]);
      spi_transfer(21, DCM_Matrix[0][1]);
      spi_transfer(22, DCM_Matrix[0][2]);
      spi_transfer(23, DCM_Matrix[1][0]);
      spi_transfer(24, DCM_Matrix[1][1]);
      spi_transfer(25, DCM_Matrix[1][2]);
      spi_transfer(26, DCM_Matrix[2][0]);
      spi_transfer(27, DCM_Matrix[2][1]);
      spi_transfer(28, DCM_Matrix[2][2]);
      #endif
}

void spi_transfer(byte code, float data){
     SPI.transfer(code);   
      
      long ldata = convert_to_dec(data);      
      for(int i=0; i<4; i++){
        SPI.transfer((byte)ldata);   
        ldata = ldata >> 8;
      }
}

long convert_to_dec(float x)
{
  return x*10000000;
}



